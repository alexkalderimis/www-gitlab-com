---
layout: markdown_page
title: "Category Vision - RubyGem Registry"
---

- TOC
{:toc}

## RubyGem Registry
A Rubygem registry offers ruby developers of lower-level services to provision an easy to use, integrated solution to share and version control ruby gems in a standardized and controlled way. Being able to provision them internally sets projects up for improved privacy and pipeline build speeds.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Rubygem%20Registry)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1293) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Our initial focus will be on providing users the ability to publish, delete and
update their Ruby dependencies within GitLab. Users will be able to authenticate
using either their personal access token or `CI_JOB_TOKEN`.
[gitlab-ee#803](https://gitlab.com/gitlab-org/gitlab-ee/issues/803) suggests
establishing a private Rubygems server for users as a possible MVC. 

## Maturity Plan
This category is currently at the "Planned" maturity level, and
our next maturity target is Minimal (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).
Key deliverables to achieve this are:

- [MVC for RubyGem Registry](https://gitlab.com/gitlab-org/gitlab-ee/issues/803)

## Competitive Landscape

- [GitHub](https://help.github.com/en/articles/configuring-rubygems-for-use-with-github-package-registry) has a product in beta that allows developers using rubygems 2.4.1 or higher to publish, delete and install packages.
- [JFrog Artifactory](https://www.jfrog.com/confluence/display/RTF/Npm+Registry)
- [Sonatype Nexus](https://www.sonatype.com/nexus-repository-sonatype)


## Top Customer Success/Sales Issue(s)
There are currently no customer success or sales issues for rubygems support.


## Top Customer Issue(s)
[gitlab-ee#803](https://gitlab.com/gitlab-org/gitlab-ee/issues/803) is currently the only request for adding support for rubygems. 


## Top Internal Customer Issue(s)
The GitLab Distribution team utilizes rubygems for downloading external dependencies. They would like to speed up their build times and remove their reliance on external dependencies by caching frequently used packages. This would require an integration with rubygems as well as the dependency proxy. [gitlab-ce#225](https://gitlab.com/gitlab-org/distribution/team-tasks/issues/225) details their needs and requirements. 

## Top Vision Item(s)
The top vision item is the MVC, [gitlab-ee#803](https://gitlab.com/gitlab-org/gitlab-ee/issues/803) which will add support for rubygems.

