---
layout: markdown_page
title: "Content schedule"
---

This list is an outline of the highest priority content scheduled. Content on this list either aligns to a content pillar, corporate marketing event, and/or brand initiative. Things on this list can change and there are things we produce that are not on this list. This is our best estimate of what to expect, when. 

Real-time updates to the blog calendar can be found on the [blog handbook page](/handbook/marketing/blog/#blog-calendar).

## FY20-Q2 (2019/05 - 2019/07)

### May

#### Editorial

#### Video

- Everyone can contribute <kbd>Video</kbd>
- Kubernetes package
- TechExplorers 1
- TechExplorers 2
- Contribute keynote
- Contribute closing
- Contribute recap 

#### Dev

- [2019 Developer Report - copy completed](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/373)

#### Ops

- [3 trends in test automation](/2019/05/01/trends-in-test-automation/)
- [GitLab's journey from Azure to GCP](/2019/05/02/gitlab-journey-from-azure-to-gcp/)
- [Kubernetes and the future of cloud native: We chat with Kelsey Hightower](/2019/05/13/kubernetes-chat-with-kelsey-hightower/) 
- Kubernetes leader 2 interview 

#### Security

- [Shift left to secure next-generation IT](/2019/05/03/secure-containers-devops/)

### June

**Brand alignment:** Contribute 2019

**Event alignment:** KubeCon

**Pillar alignment:** Reduce cycle time, Application modernization, Secure apps

#### Editorial

#### Video

#### Dev

#### Ops

#### Security

### July

#### Editorial

#### Video

#### Dev

#### Ops

#### Security

## FY20-Q3 (2019/08 - 2019/10)

### August

#### Editorial

#### Video

#### Dev

#### Ops

#### Security

### September

#### Editorial

#### Video

#### Dev

#### Ops

#### Security

### October

#### Editorial

#### Video

#### Dev

#### Ops

#### Security

## FY20-Q4 (2019/11 - 2020-01)

### November

#### Editorial

#### Video

#### Dev

#### Ops

#### Security

### December

#### Editorial

#### Video

#### Dev

#### Ops

#### Security

### January

#### Editorial

#### Video

#### Dev

#### Ops

#### Security
