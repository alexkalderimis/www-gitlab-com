---
layout: markdown_page
title: "SO.2.04 - Physical Access Role Permission Authorization Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SO.2.04 - Physical Access Role Permission Authorization

## Control Statement

Initial permission definitions, and changes to permissions, associated with physical access roles are approved by authorized personnel.

## Context

This control refers to all roles used for granting physical access to GitLab facilities.

## Scope

This control is not applicable since GitLab has no facilities.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SO.2.04_physical_access_role_permission_authorization.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SO.2.04_physical_access_role_permission_authorization.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SO.2.04_physical_access_role_permission_authorization.md).

## Framework Mapping

* ISO
  * A.11.1.5
  * A.11.1.6
* SOC2 CC
  * CC6.4
